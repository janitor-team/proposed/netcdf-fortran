netcdf-fortran (4.6.0+really4.5.4+ds-1) unstable; urgency=medium

  * QA upload
  * Revert to 4.5.4 to avoid ABI breakage, see #1016414
  * Revert 'Refresh patches'
  * Revert 'Update symbols for 4.6.0'

 -- Graham Inggs <ginggs@debian.org>  Sun, 11 Sep 2022 07:43:50 +0000

netcdf-fortran (4.6.0+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Orphan package.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 04 Sep 2022 14:11:01 +0200

netcdf-fortran (4.6.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.1, no changes.
  * Drop obsolete dh_strip override, dbgsym migration complete.
  * Update copyright file.
  * Refresh patches.
  * Update symbols for 4.6.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 30 Jul 2022 07:35:26 +0200

netcdf-fortran (4.5.4+ds-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes.
  * Update watch file for GitHub URL changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Update copyright file.
  * Refresh patches.
  * Update symbols for 4.5.4.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 08 Jan 2022 08:03:11 +0100

netcdf-fortran (4.5.3+ds-2) unstable; urgency=medium

  * Update reproducible-settings.patch with changes by Chris Lamb.
    (closes: #962401)
  * Bump watch file version to 4.
  * Update lintian overrides.
  * Mark reproducible-settings.patch as Forwarded: not-needed.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 12 Nov 2020 05:10:00 +0100

netcdf-fortran (4.5.3+ds-1) unstable; urgency=medium

  * New upstream release.
    (closes: #957593)
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
    - Don't explicitly enable autoreconf, enabled by default
    - Drop dh-autoreconf build dependency
  * Add patch to make settings file reproducible.
  * Include netcdff.settings in libnetcdff-dev.
  * Update symbols for 4.5.3.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 03 Jun 2020 06:09:02 +0200

netcdf-fortran (4.5.2+ds-1) unstable; urgency=medium

  * Bump Standards-Version to 4.4.1, no changes.
  * Drop Name field from upstream metadata.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 23 Jan 2020 05:38:53 +0100

netcdf-fortran (4.5.2+ds-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Sep 2019 07:09:46 +0200

netcdf-fortran (4.5.1+ds-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 Sep 2019 06:03:45 +0200

netcdf-fortran (4.5.0+ds-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add fortran-compiler as alternative build dependency.
    (closes: #922645)
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.
  * Update copyright file, changes:
    - Update copyright years for UCAR
    - Drop license & copyright for removed files
  * Rename library package for SONAME bump.
  * Refresh patches.
  * Update symbols for 4.5.0.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 30 Aug 2019 07:54:45 +0200

netcdf-fortran (4.4.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.3.0, no changes.
  * Update watch file to limit matches to archive path.
  * Add Build-Depends-Package field to symbols file.
  * Drop license & copyright for Autotools files.
  * Bump minimum required libnetcdf-dev to 4.6.2.
  * Refresh patches.
  * Update symbols for 4.4.5.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 10 Jan 2019 07:27:51 +0100

netcdf-fortran (4.4.4+ds-5) unstable; urgency=medium

  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 08:17:02 +0200

netcdf-fortran (4.4.4+ds-4) unstable; urgency=medium

  * Update copyright-format URL to use HTTPS.
  * Don't use libjs-jquery for Doxygen docs.
  * Bump Standards-Version to 4.1.5, no changes.
  * Update Vcs-* URLs for Salsa.
  * Strip trailing whitespace from control & rules files.
  * Drop obsolete dbg package.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Jul 2018 15:45:13 +0200

netcdf-fortran (4.4.4+ds-3) unstable; urgency=medium

  * Change priority from extra to optional.
  * Bump Standards-Version to 4.1.0, changes: priority.
  * Use pkg-info.mk variables instead of dpkg-parsechangelog output.
  * Add autopkgtest to test installability.
  * Add libnetcdf-dev to dependencies of -dev package.
  * Add patch to fix spelling errors.
  * Add patch to fix privacy-breach-generic lintian issues.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 09 Nov 2017 07:56:52 +0100

netcdf-fortran (4.4.4+ds-2) unstable; urgency=medium

  * Don't install examples, part of testsuite not real examples.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 May 2016 15:27:33 +0200

netcdf-fortran (4.4.4+ds-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8, no changes.
  * Update symbols for 4.4.4.
  * Repack upstream tarball to excluded autom4te.cache directory.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 14 May 2016 14:25:11 +0200

netcdf-fortran (4.4.3-2) unstable; urgency=medium

  * Update Vcs-Git URL to use HTTPS.
  * Bump Standards-Version to 3.9.7, no changes.
  * Enable all hardening buildflags.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 27 Mar 2016 15:46:12 +0200

netcdf-fortran (4.4.3-1) unstable; urgency=medium

  * New upstream release.
  * Add license & copyright for cfortran.h and its documentation.
  * Refresh patches, update my email to use @debian.org address.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 22 Jan 2016 11:35:49 +0100

netcdf-fortran (4.4.2-4) unstable; urgency=medium

  * Bump minimum required libnetcdf-dev to 4.4.0.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 21 Jan 2016 20:58:07 +0100

netcdf-fortran (4.4.2-3) unstable; urgency=medium

  * Limit dh_installexamples to architecture dependent packages.
    (closes: #809121)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 27 Dec 2015 14:15:43 +0100

netcdf-fortran (4.4.2-2) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Nov 2015 21:14:26 +0100

netcdf-fortran (4.4.2-2~exp1) experimental; urgency=medium

  * Update Vcs-Browser URL to use HTTPS.
  * Disable parallel test execution.
  * Bump minimum required libnetcdf-dev to 4.4.0~rc3.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 25 Oct 2015 15:32:49 +0100

netcdf-fortran (4.4.2-1) unstable; urgency=medium

  * Bump minimum required libnetcdf-dev to 4.4.0~rc2.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 19 Aug 2015 19:46:29 +0200

netcdf-fortran (4.4.2-1~exp5) experimental; urgency=medium

  * Bump minimum required libnetcdf-dev to 4.4.0~rc2-1~exp4 for serial HDF5.
  * Replace uppercase RC with lowercase rc in uversionmangle.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 31 Jul 2015 13:57:46 +0200

netcdf-fortran (4.4.2-1~exp4) experimental; urgency=medium

  * Require at least libnetcdf-dev 4.3.3.1.
  * Update watch file to handle other tar extensions in filenamemangle.
  * Move documentation build to build-indep target.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 27 Jul 2015 19:01:39 +0200

netcdf-fortran (4.4.2-1~exp3) experimental; urgency=medium

  * Add Breaks/Replaces on libnetcdf-dev to libnetcdff-dev.
    (closes: #789935)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 25 Jun 2015 19:09:57 +0200

netcdf-fortran (4.4.2-1~exp2) experimental; urgency=medium

  * Update my email to use @debian.org address.
  * Add license & copyright for Apache-2.0 licensed files.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 22 Jun 2015 21:29:18 +0200

netcdf-fortran (4.4.2-1~exp1) experimental; urgency=medium

  [ Ross Gammon ]
  * Initial release (Closes: #775524)
  * Build with latest gfortran (Closes: #751250)
  * Add fixed bug to changelog
  * Imported Upstream version 4.4.2
  * Refresh patches & drop patch applied upstream
  * Update copyright dates

  [ Bas Couwenberg ]
  * Improve initial Debian packaging.
  * Imported Upstream version 4.4.1
  * Update copyright file, add Texinfo sections.
  * Generate documentation.
  * Fix executable examples.
  * Add patch to fix privacy-breach-logo lintian error,
    by using the local logo image.
  * Fix netcdf_fortran.3 man page install.
  * Add patch to fix hyphen-used-as-minus-sign issues in man page.
  * Add symbols file for libnetcdff.
  * Mark hyphen-used-as-minus-sign.patch as Forwarded.
  * Mark hyphen-used-as-minus-sign.patch as Applied-Upstream.
  * Install RELEASE_NOTES.md as upstream changelog.

 -- Ross Gammon <rossgammon@mail.dk>  Sat, 14 Feb 2015 00:20:06 +0100
